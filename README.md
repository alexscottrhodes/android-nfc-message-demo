# Android NFC Demo#

    Android NFC Demo
    Copyright (C) 2017  Alex Rhodes
    https://www.alexrhodes.io

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.
    Thermostat for Echo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    The Android Robot logo and NFC logo are used under their respective 
    Creative Commons licenses.

This application provides a skeleton for NFC communication between two applications. Two different NFC enabled devices are able to send and receive string messages back and forth using NFC. The purpose of this application is to demonstrate basic NFC usage for inter-device communication and to provide a foundation for applications that require NFC operations. 

The main components are:

* [the Main Activity](https://bitbucket.org/alexscottrhodes/android-nfc-message-demo/src/cdb312eab878451ffa26f3afc7ec5a4788d2f3de/app/src/main/java/alexrhodes/io/nfcdemo/MainActivity.java?fileviewer=file-view-default)

* [the Manifest with permissions and Intent Filter](https://bitbucket.org/alexscottrhodes/android-nfc-message-demo/src/cdb312eab878451ffa26f3afc7ec5a4788d2f3de/app/src/main/AndroidManifest.xml)

Those two files are heavily commented to describe the code.

##Basic Usage##
Deploy the application on to two separate Android devices. The devices must:

* have NFC hardware 

* have NFC features enabled

API version 14 or higher is required for many NFC related features.

1. Open the application on Phone A. Type a message into the message box in the UI. Press the "Back" button to close the keyboard, or the Android Beam will not start.
2. Unlock Phone B and hold the phones back-to-back or in the best position for NFC. 
3. Phone A will prompt the user to begin the Android Beam. The message will be pushed to Phone B and Phone B will automatically open the application and display the message that was transmitted from Phone A.
4. If both phones are running the application, they will both prompt the user to begin the Android Beam and messages can be sent back and forth.

   
```
 Alex Rhodes
 alex@alexrhodes.io
```