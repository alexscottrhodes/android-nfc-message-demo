package alexrhodes.io.nfcdemo;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.TextView;

import static android.nfc.NdefRecord.createMime;


/*
    Main activity that runs at start up, implements Ndef Message Callback to create NFC messages
    when beam is started. Both phones running this application can send or receive, and will be prompted to send the
    beam a the same time. These two tasks could be broken up into two different apps, one to send messages, and one
    that receives and processes them. 
 */
public class MainActivity extends Activity implements NfcAdapter.CreateNdefMessageCallback
{
    //Will hold the default device NFC adapter
    private NfcAdapter adapter;
    private TextView output;

    //Main activity "constructor"
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Output message label
        output = (TextView)findViewById(R.id.outputMessage);

        //Set the NfcAdapter to the default device NFC chip
        adapter = NfcAdapter.getDefaultAdapter(this);

        //If this is null, the phone doesn't have NFC
        if(adapter == null){
            //Add some not supported message
            output.setText("This device does not support NFC");
            return;
        }
        //If the adapter is disabled, NFC won't work
        if(!adapter.isEnabled()){
            //Add a "Please enable NFC" type message etc.
            output.setText("NFC is not enabled");
            return;
        }
        //Register the message creation callback
        adapter.setNdefPushMessageCallback(this, this);
        //If the activity was cold-launched by a nearby NFC device, we still want to handle the Ndef Message
        handleIntent(getIntent());
    }

    //Overridden from Activity, we want to foreground dispatch the NFC intent so that we don't keep starting new activities
    @Override
    public void onResume() {
        super.onResume();
        //Setting up the foreground dispatch
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        adapter.enableForegroundDispatch(this, pendingIntent, null, null);
        //Handle the intent
        handleIntent(getIntent());
    }

    //Also from activity, we want to handle the intent in any logical event handler of the activity
    @Override
    public void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    /**
     * This method takes in every intent that is passed to the activity, it filters for the ACTION_NDEF_DISCOVERED
     * intents that indicate an NFC event.
     */
    void handleIntent(Intent intent) {
        //If the intent has NFC Ndef message received
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            //The following pulls the Array of messages from the intent
            Parcelable[] rawMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if ((rawMessages != null)) {
                //In this example, we take the first message in the array, get it's Array of Ndef Records, take the first
                //record, and grab it's payload. The payload contains the string being passed.
                String recMsg = new String(((NdefMessage)rawMessages[0]).getRecords()[0].getPayload());
                //Display the message
                output.setText(recMsg);
                return;
            }
        }
    }

    //Overridden from the CreateNdefCallback parent class, this creates the message we want to send when the beam starts
    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        /*
            For this example, text is the message the user entered into the input text. However,
            this is really the right place to determine a standardized model for the messages to be sent
            and then serialize them into a widely used format like JSON etc. For now, it's just a plain string
         */
        String text = ((TextView) findViewById(R.id.inputMessage)).getText().toString();

        //The mime type here has to match the mime type in the manifest intent filter
        NdefMessage msg = new NdefMessage(
                new NdefRecord[] {
                        createMime("application/vnd.io.alexrhodes", text.getBytes())});
        return msg;
    }


}
